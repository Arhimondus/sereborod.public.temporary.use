var bluebird = require('bluebird');
var pgp = require('pg-promise')();
var cfg = require('./server.cfg.js');
var db = pgp(cfg.db);
var bcrypt = require('bcryptjs');
var shortid = require('shortid');
var uuid = require('uuid/v4');
var captchaGenerator = require('trek-captcha');
var redis = require('redis'), client = bluebird.promisifyAll(redis.createClient());
var sereborod = require('./sereborod.js');
var loki = require('lokijs');

//var sereborod = require('./sereborod.js');
//sereborod.Battle

//SELECT * FROM battles WHERE heroes @> '[{"id":2}]';
//https://habrahabr.ru/post/254425/

module.exports.setup = function(app, io) {
	async function current_user(req) {
		var token = req.query.token || null;
		if(token == null)
			throw new Error("Отсутствует токен");
		var [uid,_] = (await client.getAsync(token)).split(",").map((v) => v-0);
		if(uid == null)
			throw new Error("Неверный токен");
		return uid;
	}
	async function current_hero(req) {
		var token = req.query.token || null;
		if(token == null)
			throw new Error("Отсутствует токен");
		var [uid,hid] = (await client.getAsync(token)).split(",").map((v) => v-0);
		if(uid == null)
			throw new Error("Неверный токен");
		if(!hid) throw new Error("Герой не был выбран");
		return hid;
	}
	//disconnect
	io.use(async function(socket, next) {
		var token = socket.handshake.query.token;
		if (!token)
			return auth.fail("Токен отсутствует", data, accept);
			
		var [uid, hid] = (await client.getAsync(token)).split(",").map((v) => v-0);
		if(uid && hid) {
			socket.hero_id = hid;
			next();
		}
		else {
			next("Неверный токен");
		}
	});

	function hero_info(id, name) {
		this.id = id;
		this.name = name;
		this._rawDBType = true;
		this.formatDBType = function () { 
			return pgp.as.format('($1, $2)::hero_info', [this.id, this.name]);
		}
	}
	
	var battles = new Map();
	function new_battle(battle) {
		var db = new loki('battle_' + battle.id);	
		var hero_data = db.addCollection('hero_data', { unique: ['hero_id'], autoupdate: true }); // { hero_id, team_id, params, slot, skills ... }
		var game_state = db.addCollection('game_state');
		game_state.insert({ round: 1 });
		for(var i = 0; i < battle.heroes.length; i++)
			hero_data.insert({ 
				hero_id: battle.heroes[i].id,
				team_id: (i % 2) + 1
			});
		battles.set(battle.id, db);
	}
	
	io.on('connection', function(socket) {
		console.log(`a hero #[${socket.hero_id}] connected at socket ${socket.id}`);
		socket.on('game/data', async function(fn){
			var hero_id = socket.hero_id;
			var data = await db.one(`SELECT data FROM battles WHERE status = 2 AND heroes @> '[{"id": $<hero_id>}]' RETURNING data`, {
				hero_id,
			});
			fn(data);
		});
		socket.on('game/slot/set', async function(slot) {
			db.query(`UPDATE battles SET data = data || '[{"id":$<id^>, "name":"$<name^>"}]'::jsonb WHERE id = $<battle_id>`,{
				id: hero.id,
				name: hero.name,
				battle_id: battle.id,
			});
		});
		socket.on('hero/current_battle', async function(fn) {
			var hero_id = socket.hero_id;
			var battle = await db.oneOrNone(`SELECT id, status FROM battles WHERE (status = 1 OR status = 2) AND heroes @> '[{"id": $<hero_id>}]'`, {
				hero_id,
			});
			fn(battle);
		});
		socket.on('battle/create', async function(battle) {
			try {
				var hero = await db.one('SELECT id, name FROM heroes WHERE id = ${id} LIMIT 1', {					
					id: socket.hero_id
				});
				if(!hero)
					throw new Error("Неверный герой?");
				
				var battles = await db.query(`SELECT id FROM battles WHERE (status = 1 OR status = 2) AND heroes @> '[{"id": $<id>}]'`, {
					id: hero.id,
				});
				if(battles.length > 0)
					throw new Error("Герой уже в активной заявке!");

				var battle = { status: 1 };
				battle.heroes = [{ id: hero.id, name: hero.name }];
				battle.id = (await db.one('INSERT INTO battles(mode, params, heroes) VALUES(${mode}, ${params}, ${heroes}) RETURNING id', {
					mode: battle.mode,
					params: battle.params,
					heroes: JSON.stringify(battle.heroes),
				})).id;
				socket.emit('battle/create/success', battle);
				socket.broadcast.emit('battle/create', battle);
				//io.emit('battle/create', battle);
			} catch({ message }) {
				socket.emit('battle/create/error', message);
				console.log("ERROR battle/create " + message);
			}
		});
		socket.on('battle/join', async function(battle_id) {
			try {
				var hero = await db.one('SELECT id, name FROM heroes WHERE id = ${id} LIMIT 1', {					
					id: socket.hero_id
				});
				if(!hero)
					throw new Error("Неверный герой?");
				var battles = await db.query(`SELECT id FROM battles WHERE (status = 1 OR status = 2) AND heroes @> '[{"id": $<id>}]'`, {
					id: hero.id,
				});
				if(battles.length > 0)
					throw new Error("Герой уже присутствует в другом бою!");
				var battle = await db.one('SELECT * FROM battles WHERE id = ${battle_id} AND status = 1 LIMIT 1', {					
					battle_id
				});
				var mode = sereborod.Modes[battle.mode];
				if(!mode.check_requirements(hero, battle)) //Включая вместимость - здесь?
					throw new Error("Герой не подходит под требования мода(режима)!");	
				battle.heroes.push({ id: hero.id, name: hero.name });
				//ДАЛЕЕ КРАЙНЕ ВАЖНА КРЫШЕЧКА!!! Она необработанные переменные вставляет. Узнать почему не работает без неё!
				if(mode.is_mode_ready(battle)) battle.status = 2;
				await db.query(`UPDATE battles SET status = $<status>, heroes = heroes || '[{"id":$<id^>, "name":"$<name^>"}]'::jsonb WHERE id = $<battle_id>`,{
					status: battle.status,
					id: hero.id,
					name: hero.name,
					battle_id: battle.id,
				});
				
				socket.emit('battle/join/success', battle);
				socket.broadcast.emit('battle/change', battle);
			} catch({ message }) {
				socket.emit('battle/join/error', message);
				console.log("ERROR battle/create " + message);
			}
		});
		socket.on('battle/leave', async function(battle_id) {
			try {
				var hero = await db.one('SELECT id, name FROM heroes WHERE id = ${id} LIMIT 1', {					
					id: socket.hero_id
				});
				if(!hero)
					throw new Error("Неверный герой?");
				var battles = await db.query(`SELECT * FROM battles WHERE id = $<battle_id> AND status = 1 AND heroes @> '[{"id": $<id>}]' LIMIT 1`, {					
					battle_id: battle_id,
					id: hero.id,
				});
				if(battles.length == 0)
					throw new Error("Герой пытается покинуть бой, в котором его нету!");
				var battle = battles[0];
				battle.heroes = battle.heroes.filter((f) => f.id != hero.id);
				if(battle.heroes.length > 0) {
					await db.query('UPDATE battles SET heroes = ${heroes} WHERE id = $<battle_id>', {
						heroes: JSON.stringify(battle.heroes),
						battle_id: battle.id,
					});
					socket.broadcast.emit('battle/change', battle);
				}
				else {
					await db.query('DELETE FROM battles WHERE id = $<battle_id>', {
						battle_id: battle.id
					});
					io.emit('battle/remove', battle);
				}
				socket.emit('battle/leave/success', battle);
			} catch({ message }) {
				socket.emit('battle/leave/error', message);
				console.log("ERROR battle/leave " + message);
			}
		});
	});

	/**
	* @swagger
	* definitions:
	*   Target:
	*     required:
	*       - id
	*       - fake
	*       - dang
	*       - votes
	*     properties:
	*       id:
	*         type: string
	*       fake:
	*         type: integer
	*       dang:
	*         type: integer
	*       votes:
	*         type: integer
	*/
   
	/**
	* @swagger
	* /register:
	*   post:
	*     description: Регистрация
	*     parameters:
	*     - name: name
	*       description: Имя
	*       in: formData
	*       required: true
	*       type: string
	*     - name: login
	*       description: Логин
	*       in: formData
	*       required: true
	*       type: string
	*     - name: email
	*       description: Почта
	*       in: formData
	*       required: true
	*       type: string
	*     - name: identifier
	*       description: Идентификатор сессии
	*       in: formData
	*       required: true
	*       type: string
	*     - name: token
	*       description: Токен безопасности
	*       in: formData
	*       required: true
	*       type: string
	*     - name: password
	*       description: Пароль
	*       in: formData
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Успешная регистрация		 
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/@/register", async function(req, res) {
		try {
			var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
			
			var identifier = req.body.identifier;
			var registerToken = req.body.token;
			
			var trueToken = await client.getAsync(`sbd:register:${ip}:${identifier}`);
			if(trueToken == null || registerToken != trueToken)
				return res.status(400).send("Неверный токен безопасности!");
			await client.delAsync(`sbd:register:${ip}:${identifier}`);
			
			var raw_password = req.body.password;
			var name = req.body.name;
			var email = req.body.email;
			var login = req.body.login;
			
			var password = bcrypt.hashSync(raw_password, 10);
			
			var code = shortid.generate();
			
			var x = db.one('INSERT INTO accounts(name, email, login, password, code) VALUES(${name}, ${email}, ${login}, ${password}, ${code}) RETURNING id', {
				name, email, login, password, code
			}).then(async data => {
				var uid = data.id;
				var token = uid + "_" + uuid();
				await client.setAsync(token, data.id + ",0");
				//await client.expire(token, 300);
				return res.json({ uid, name, token, });
			}).catch(error => {
				return res.status(400).json(error);
			});	
		} catch(ex) {
			res.status(400).send(ex);
		}
	});
	
	/**
	* @swagger
	* /register/captcha:
	*   get:
	*     description: Запрос идентификатора и капчи для регистрации 
	*     responses:
	*       200:
	*         description: Получена base64 капча и идентификатор	 
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.get("/@/register/captcha", async function(req, res) {
		try {
			var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
		
			var keys = await client.scanAsync('0', 'MATCH', `sbd:register:${ip}:*`, 'COUNT', '200');
			if(keys[1].length >= 100) 
				return res.status(423).send("Превышен лимит запросов для IP адреса");
			
			var identifier = shortid.generate();
			var { token, buffer } = await captchaGenerator();
			await client.setAsync(`sbd:register:${ip}:${identifier}`, token, 'EX', 240);
			var captcha = buffer.toString('base64');
			
			res.json({ identifier, captcha });
		} catch(ex) {
			res.send(400, ex);
		}
	});
	
	/**
	* @swagger
	* /login:
	*   post:
	*     description: Авторизация
	*     parameters:
	*     - name: login
	*       description: Логин
	*       in: formData
	*       required: true
	*       type: string
	*     - name: password
	*       description: Пароль
	*       in: formData
	*       required: true
	*       type: string
	*       format: password
	*     responses:
	*       200:
	*         description: Успешная авторизация	(возвращает строковую сессию)
	*       404:
	*         description: Пользователь не найден
	*       401:
	*         description: Неверный пароль
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/@/login", function(req, res) {
		try {
			var login = req.body.login;
			var password = req.body.password;
			
			db.one('SELECT * FROM accounts WHERE login = ${login} LIMIT 1', {
				login,
			}).then(async account => {
				if(!bcrypt.compareSync(password, account.password))
					return res.send(401, "Неверный пароль");
				var token = account.id + "_" + uuid();
				await client.setAsync(token, account.id + ",0");
				//await client.expire(token, 300);
				//Добавить просто этот токен в саму базу постгрес, что такой токен есть и связан с этим пользователем
				res.json({
					token: token,
					name: account.name,
					uid: account.id,
					heroes_limit: account.heroes_limit,
				});
			}).catch(error => {
				//res.status(400).json(error);
				res.status(404).send("Пользователь не найден");
			});
		} catch(ex) {
			res.status(400).send(ex);
		}
	});

	/**
	* @swagger
	* /target/{id}:
	*   get:
	*     description: Получить список открытых боёв
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Цель найдена
	*         schema:
	*           type: object
	*           $ref: '#/definitions/Target'
	*       404:
    *         description: Цель не найдена
	*       400:
    *         description: Что-то пошло не так	
	*/
	//pgp.pg.types.setTypeParser(24922, function() {});
	app.get("/@/battle/status/open", async function(req, res) {
		try {
			var battles = await db.query('SELECT id, status, mode, params, heroes FROM battles WHERE status = 1');
			res.json(battles);
		} catch(ex) {
			console.log(ex);
			res.status(400).send(ex);
		}
	});

	/**
	* @swagger
	* /target/{id}:
	*   get:
	*     description: Получить список открытых боёв
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Цель найдена
	*         schema:
	*           type: object
	*           $ref: '#/definitions/Target'
	*       404:
    *         description: Цель не найдена
	*       400:
    *         description: Что-то пошло не так	
	*/
	/*app.post("/battle/create", async function(req, res) {
		try {
			var battles = (await client.scanAsync('0', 'MATCH', `sbd:open_battles:*`, 'COUNT', '20'))[1];
			res.send(JSON.stringify(battles));
		} catch(ex) {
			res.send(400, ex);
		}
	});*/
	
	/**
	* @swagger
	* /target/{id}:
	*   get:
	*     description: Получить список открытых боёв
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Цель найдена
	*         schema:
	*           type: object
	*           $ref: '#/definitions/Target'
	*       404:
    *         description: Цель не найдена
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.get("/@/hero/list", async function(req, res) {
		try {
			var uid = await current_user(req);
			var heroes = await db.query('SELECT id, name, class, level FROM heroes WHERE account_id = ${uid}', {
				uid
			});
			res.json(heroes);
		} catch(ex) {
			res.status(400).send(ex);
		}
	});

	/**
	* @swagger
	* /target/{id}:
	*   get:
	*     description: Создать нового героя
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Цель найдена
	*         schema:
	*           type: object
	*           $ref: '#/definitions/Target'
	*       404:
    *         description: Цель не найдена
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/@/hero/create", async function(req, res) {
		try {
			var token = req.query.token || null;
			if(token == null)
				throw new Error("Отстутствует токен");
			var [uid,_] = (await client.getAsync(token)).split(",").map((v) => v-0);
			if(uid == null)
				throw new Error("Неверный токен");
			var hero_id = (await db.one('INSERT INTO heroes(account_id, class, name) VALUES(${account_id}, ${class}, ${name}) RETURNING id', {
				account_id: uid,
				class: req.body.class,
				name: req.body.name,
			})).id;
			res.json(hero_id);
		} catch(ex) {
			res.status(400).send(ex);
		}
	});
	
	/**
	* @swagger
	* /target/{id}:
	*   get:
	*     description: Выбрать героя
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Цель найдена
	*         schema:
	*           type: object
	*           $ref: '#/definitions/Target'
	*       404:
    *         description: Цель не найдена
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.post("/@/hero/select", async function(req, res) {
		try {
			var token = req.query.token || null;
			if(token == null)
				throw new Error("Отстутствует токен");
			var [uid, _] = (await client.getAsync(token)).split(",").map((v) => v-0);
			if(uid == null)
				throw new Error("Неверный токен");
			var hero = await db.one('SELECT * FROM heroes WHERE account_id = ${account_id} AND id = ${id} LIMIT 1', {
				account_id: uid,
				id: req.body.id,
			});
			if(hero.current_battle)
				hero.battle = await db.one('SELECT id, status FROM battles WHERE id = ${current_battle} AND (status = 1 OR status = 2)', {
					current_battle: hero.current_battle,
				});
			else {
				//Пытаемся найти героя в уже существующем бою!
				var battles = await db.query(`SELECT id, status FROM battles WHERE (status = 1 OR status = 2) AND heroes @> '[{"id": $<id>}]' LIMIT 1`, {					
					id: hero.id,
				});
				if(battles.length > 0) {
					hero.battle = battles[0];
					//Нужно ли проставлять current_battle?
				}
			}
			await client.setAsync(token, uid + "," + hero.id);
			res.json(hero);
		} catch(ex) {
			res.status(400).send(ex);
		}
	});
	
	/**
	* @swagger
	* /target/{id}:
	*   get:
	*     description: Просмотр героя
	*     parameters:
	*     - name: id
	*       description: Идентификатор цели
	*       in: path
	*       required: true
	*       type: string
	*     responses:
	*       200:
	*         description: Цель найдена
	*         schema:
	*           type: object
	*           $ref: '#/definitions/Target'
	*       404:
    *         description: Цель не найдена
	*       400:
    *         description: Что-то пошло не так	
	*/
	app.get("/@/hero/view", async function(req, res) {
		try {
			var token = req.query.token || null;
			if(token == null)
				throw new Error("Отстутствует токен");
			var [uid, hid] = (await client.getAsync(token)).split(",").map((v) => v-0);
			if(uid == null)
				throw new Error("Неверный токен");
			if(hid == null)
				throw new Error("Не выбран герой");
			var hero = (await db.one('SELECT id FROM heroes WHERE account_id = ${account_id} AND id = ${id} LIMIT 1', {
				account_id: uid,
				id: req.body.id,
			}));
			if(hero.length != 1)
				throw new Error("Неверный герой?");
			res.status(200).json(hero);
		} catch(ex) {
			res.status(400).send(ex);
		}
	});
	
	app.get("/$/sereborod.js", async function(req, res) {
		res.file("sereborod.js");
	});
	
	app.get("/$/hero/add_experience", async function(req, res) {
		var hero_id = req.query.hero_id;
		var experience = req.query.experience;
		var new_experience = await db.one('UPDATE heroes SET experience = experience + $<experience> WHERE id = $<hero_id> RETURNING experience', {
			experience,
			hero_id,
		});
		res.send(new_experience);
	});
};