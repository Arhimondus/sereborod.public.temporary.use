﻿/*var loki = require('lokijs');
var db = new loki('b1s.json');
var collection = db.addCollection('abc', { indices:['email'], autoupdate: true });
collection.insert({ b: 5, email: 'afasfs', array: [1, 2, 3] });
var g = collection.get(1);
console.log(g);
g.b = 6;
g.array.push(666);
g.yogurt = 500;
var c = collection.get(1);
console.log(c);
return;*/
const express = require('express'),
	bodyParser = require('body-parser'),
	swagger = require('restify-swagger-jsdoc'),
	app = express(),
	http = require('http').Server(app),
	io = require('socket.io')(http),
	port = require('yargs').argv.port;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('client'));
 
require('./api').setup(app, io);

swagger.createSwaggerPage({
    title: 'API documentation', // Page title (required) 
    version: '1.0.0', // Server version (required) 
    server: app, // Restify server instance created with restify.createServer() 
    path: '/docs/swagger', // Public url where the swagger page will be available 
    apis: [ `${__dirname}/api.js` ], // Path to the API docs 
    //routePrefix: null // prefix to add for all routes (optional) 
});

http.listen(port, function () {
	console.log(`[[sereborod game-server]] started on port ${port}`);
});