/// <reference path="Battle.ts" />
/// <reference path="Skill.ts" />
// Hero.ts
module Sereborod {
    export enum EParamType {
		Сила,
		Ловкость,
		Интеллект,
		Выносливость,
		Удача,
		ФизическаяЗащита,
		МагическаяЗащита,
		ПовышениеУрона,
		ПовышениеЗащиты,
		Баффы
	}

    export class Param {
		type: EParamType;
		value: Value[];

		get total_value(): number {
            var absoluteValues = this.value.filter(v => v.number_type == ValueNumberType.Абсолютное).map(v => v.value);
            var av = 0;
            if ( absoluteValues.length > 0 )
                av = absoluteValues.reduce((prev, curr) => prev + curr);

            var percentageValues = this.value.filter(v => v.number_type == ValueNumberType.Процентное).map(v => v.value);
            var pv = 0;
            if ( percentageValues.length > 0 )
                pv = percentageValues.reduce((prev, curr) => prev + curr);

            av = av + av * pv / 100;

            return av;
		}

		constructor(type: EParamType = null, initial: Value = null) {
            if(type != null)
                this.type = type;
            if(initial != null)
                this.value.push(initial);
		}

		get hard_value(): number {
			var vs = this.value.find( f => f.type == ValueType.Параметр );
			return vs != null ? vs.value : 0;
		}
        set hard_value(value) {
            var vs = this.value.find( f => f.type == ValueType.Параметр );
            vs.value = value;
        }

		get items_value(): number {
			var vs = this.value.find( f => f.type == ValueType.Предмет );
			return vs != null ? vs.value : 0;
		}

		get ability_value(): number	{
			var vs = this.value.find( f => f.type == ValueType.ПассивнаяСпособность );
			return vs != null ? vs.value : 0;
		}

		get permanent_value(): number {
			return this.hard_value + this.items_value + this.ability_value;
		}

		get float_value(): number {
			var vs = this.value.find( f => f.type == ValueType.АктивнаяСпособность );
			return vs != null ? vs.value : 0;
			//return TotalValue - HardValue - ItemsValue;
		}
	}

    enum ValueType {
		Параметр,
		Предмет,
		АктивнаяСпособность,
		ПассивнаяСпособность
	};

    enum ValueNumberType {
		Абсолютное,
		Процентное
	};

	class Value {
		value: number;
		type: ValueType;
		number_type: ValueNumberType;

		//[NonSerialized]
		from: Hero;
		//[NonSerialized]
		to: Hero;
		//[NonSerialized]
		skill: Skill;
		//[NonSerialized]
		continue_data: any;
		continue_action: boolean;
		skill_var_id: number;
		time: number = -1;
	}
	class HeroType {
		skills: Skill[];
	}
    export class HeroParams {
        agility: number;
        strength: number;
    }

    export class HeroAbilityActive extends Skill {		
		naked: IActiveNakedSkill;
		get action_points() {
			return this.naked.action_points[this.level];
		};
		get mana_cost() {
			return this.naked.mana_cost[this.level];
		};
		get cooldown() {
			return this.naked.cooldown[this.level];
		};
		get step_duration() {
			return this.naked.step_duration[this.level];
		};
		get minimal_round() {
			return this.naked.minimal_round[this.level];
		};
		get up_level() {
			return this.naked.up_level[this.level];
		};
		apply(battle: Battle, targets: any) {
			this.naked.apply(battle, targets, this);
		};
    }
    export class HeroAbilityPassive extends Skill {
		naked: IPassiveNakedSkill;
        custom: boolean;
        d: number;
		constructor(skill_id, owner, params) {
			super(skill_id, owner, params);
			this.naked = Sereborod.Passives[skill_id];
		};
		apply(battle: Battle, eventType: string, eventParams: any) {
			this.naked.apply(battle, eventType, eventParams, this);
		};
    }
    export class Hero extends HeroParams {
		id: number;
		name: number;
		alive: boolean;
		death_type: string;
		slots: Sereborod.ISlot[];
		params: Param[];
		health: number;
		max_health: number;
		energy: number;
		max_energy: number;
		abilities_active: HeroAbilityActive[];
		abilities_passive: HeroAbilityPassive[];
		side: number; //номер команды/стороны
		preInitHero() {
			this.abilities_active
				.filter(a => a.naked.initialize)
				.forEach((a) => a.naked.initialize(a, this));
			this.abilities_passive
				.filter(a => a.naked.initialize)
				.forEach((p) => p.naked.initialize(p, this));
		};
		get Сила() {
			return this.params.find(f => f.type == EParamType.Сила); 
		};
		get Ловкость() {
			return this.params.find(f => f.type == EParamType.Ловкость); 
		};
		get Интеллект() {
			return this.params.find(f => f.type == EParamType.Интеллект); 
		};
		get Удача() {
			return this.params.find(f => f.type == EParamType.Удача); 
		};
		get Выносливость() {
			return this.params.find(f => f.type == EParamType.Выносливость); 
		};
		get ФизическаяЗащита() {
			return this.params.find(f => f.type == EParamType.ФизическаяЗащита); 
		};
		get МагическаяЗащита() {
			return this.params.find(f => f.type == EParamType.МагическаяЗащита); 
		};
		get ПовышениеУрона() {
			return this.params.find(f => f.type == EParamType.ПовышениеУрона); 
		};
		get ПовышениеЗащиты() {
			return this.params.find(f => f.type == EParamType.ПовышениеЗащиты); 
		};
		get Баффы() {
			return this.params.find( f => f.type == EParamType.Баффы);
		};
    }
}