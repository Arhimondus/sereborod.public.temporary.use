/// <reference path="../Battle.ts" />
/// <reference path="../Skill.ts" />
module Sereborod {
	export const Actives: { [key:string]:IActiveNakedSkill } = {
		"1": {
			id: 1,
			class: "maroder",
			name: "Удар мародёра",
			//description: "Восстанавливает ${logic.byLevelParam()*100} от нанесённого урона физическими атаками в здоровье.",
			description: "Наносит ${1} ед. урона + ${2} от силы. В случае если удар завершиться смертью цели, то мародёр получит за бой золота на %{3} больше.",
			params: [[5, 10, 15], [10], [0.10]],

			mana_cost: [4],			
			action_points: [1],
			cooldown: [0], 
			step_duration: [0],
			minimal_round: [0],
			
			type: ESkillType.physicalAttack,
			targets: [{ unit_number: 1, target_type: ESkillTargetType.enemy }],
			up_level: [1, 5, 10],

			apply(battle: Battle, targets: any, $) {
				
			},
		},
		"2": {
			id: 2,
			class: "maroder",
			name: "Стремительная атака",
			description: "Наносит урон в размере ${1}% от силы, в случае если у противника остаётся менее 30-60%(основано на Удаче) здоровья, то парализует цель на ${2} очко действия.",
			params: [[20, 35, 50], [1]],

			mana_cost: [6],
			action_points: [2], 
			cooldown: [1], 

			type: ESkillType.physicalAttack,
			targets: [{ unit_number: 1, target_type: ESkillTargetType.enemy }],
			up_level: [2, 6, 12],

			apply(battle: Battle, targets: any, $) {
				
			},
		}
	}
}