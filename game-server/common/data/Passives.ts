/// <reference path="../NakedSkill.ts" />
module Sereborod {
	export const Passives: { [key:string]:IPassiveNakedSkill } = {
		"1": {
			id: 1,
			name: "Вампиризм",
			description: "Восстанавливает ${logic.byLevelParam()*100} от нанесённого урона физическими атаками в здоровье.",
			params: [[0.075, 0.150, 0.225]],
			events: ["ПослеНанесенияУронаПерсонажу"],
			apply(battle, eventType, eventParams, $) {
				return (eventType == "ПослеНанесенияУронаПерсонажу" && $.owner == eventParams.initiator)
				? (
					$.restoreHealth(eventParams.initiator, eventParams.initiator, ESkillType.physicalSkill, eventParams.Damage * $.byLevelParam()),
					true
				)
				: false;
			},
		},
		"2": {
			id: 2,
			name: "Критический удар",
			description: "Вероятность ${logic.byLevelParam()*100}% нанести в два раза больше урона физическими атаками.",
			events: ["ДоНанесенияУронаПерсонажу"],
			params: [[0.10, 0.15, 0.20]],
			apply(battle, eventType, eventParams, $) {
				return (eventType == "ДоНанесенияУронаПерсонажу" && $.owner == eventParams.initiator) && ($.random_f() < $.byLevelParam()) ? (
					eventParams["ПредполагаемыйУрон"] *= 2,
					$
				) : false;
			},
		},
		"3": {
			id: 3,
			name: "Дьявольский удар",
			description: "Вероятность 2-7%(основано на Ловкости) нанести в ${logic.byLevelParam(2, [[3,'раза'],[4,'раза'],[5,'раз']])} больше урона физическими атаками.",
			events: ["ДоНанесенияУронаПерсонажу"],
			params: [[3, 4, 5]],
			apply(battle, eventType, eventParams, $) {
				return ((eventType == "ДоНанесенияУронаПерсонажу" && $.owner == eventParams.initiator) &&
					($.random() < $.baseOnParam($.getHeroParam(EParamType.Ловкость), [
						[0, 2],
						[30, 3],
						[60, 4],
						[90, 5],
						[120, 6],
						[150, 7]
					]))) ? (
					eventParams.modifiers.push($),
					eventParams["ПредполагаемыйУрон"] *= $.byLevelParam(),
					true
				) : false;
			},
		},
		"35": {
			id: 35,
			name: "Здоровье",
			description: "Увеличивает на каждую единицу Выносливости здоровье на {0} ед.",
			params: [[1.22, 1.88, 2.54]],
			initialize(skill: Skill, hero: Hero) {
				hero.max_health = 15 + hero.params.find(f=>f.type == EParamType.Выносливость).value[0].value * skill.byLevelParam();
			},
		}
	};
}