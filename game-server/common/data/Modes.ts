/// <reference path="../Hero.ts" />
/// <reference path="../Battle.ts" />
/// <reference path="../_Battle.ts" />
module Sereborod {
	interface IMode {
		name: string;
		check_requirements(hero: Hero, battle: Battle);
		is_mode_ready(battle: DbBattle);
	}
	export const Modes: { [key:string]:IMode } = {
		"default": {
			name: "Тестовый",
			check_requirements(hero: Sereborod.Hero, battle: Battle) {
				return true; 
			},
			is_mode_ready(battle: DbBattle) {
				return (battle.heroes.length == 2);
			}
		},
		"standard": {
			name: "Обычный",
			check_requirements(hero: Sereborod.Hero, battle: Battle) {
				return true;
			},
			is_mode_ready(battle: DbBattle) {
				return (battle.heroes.length == battle.params);
			}
		}
	};
}