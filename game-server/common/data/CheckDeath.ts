module Sereborod {
    export const CheckDeath: any = {
        default(battle_data_hero, battle = null) {
            return battle_data_hero.abilities_passive[35].health <= 0;
        },
        raid_boss(battle_data_hero, battle = null) {
            return false;
        },
    };
}