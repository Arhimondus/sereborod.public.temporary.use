﻿/// <reference path="Battle.ts" />
/// <reference path="NakedSkill.ts" />
//var bindAll = require('bindall-standalone');
namespace Sereborod {
	export interface IEventParam {
		target?: Hero;
		initiator?: Hero;
		skill?: Skill;
		damage?: IntegerRef;
		damage_type?: any;
		supposed_damage?: IntegerRef;
	}
	
	export function D3(numbers: number[]) {
		//Функция конвертации раундов в этапы
		return numbers.map(m => m * 3);
	}
	class IntegerRef {
		value: number;
		constructor(value: number) {
			this.value = value;
		}
	}
	export enum ESkillType {
		physicalSkill = 1,
		physicalAttack = 2,
		physicalDamage = 3,
		/*+*/
		magicalSkill = 4,
		magicalAttack = 5,
		magicalDamage = 6,
		/*+*/
		clearDamage = 7,
		
		mentalAttack = 8,
		mentalDamage = 9, 
		mentalSkill = 10,
		/*+*/
		mentalPurge = 11,
	};
	export interface ISkillTarget {
		unit_number: number;
		target_type: ESkillTargetType;
	}
	export enum ESkillTargetType {
		no_target = 0,
		any_unit = 1,
		ally = 2,
		enemy = 3,
		neutral = 4,
		ally_no_cur = 5,
	}
	export class Skill {
		id: number;
        level: number;
        
		naked: INakedSkill;
		owner: Hero;
		params: any;

		cooldown_timer: number = 0;

		constructor(skill_id, owner, params) {
			
		};
		getHeroParam(type: EParamType): number {
			return this.owner.params.find(p => p.type == type).total_value;
		};
		byLevelParam(number: number = 1, based_word/*: double_array[number, string]*/ = null): number {
			//this == sp => { skill, params, owner }
			var calculated = this.naked.params[number][this.level - 1];
			if(based_word != null) {
				//based_word = [[1, "раз"]], [2, "раза"], [9, "раз"]]
				var item = based_word.find((i) => i[0] == calculated);
				if(item != null) calculated += item[1];
			}
			return calculated;
		};
		restoreHealth(initiator, target, skill_type, value) {
			return 10;
		};
		baseOnParam(param_number: number, range_numbers: number[][]): number {
			let modifier = 0;
			for(var [check, set] of range_numbers.reverse()) {
				if ( param_number >= check ) {
					modifier = set;
					break;
				}
			}
			return modifier;
		};
		random(number = 101): number { //100%
			return this.random_f() * 100;
		};
		random_f(number = 1.1): number { //1
			return Math.random();
		};
		makeDamage(battle: Battle, from: Hero, to: Hero, type: ESkillType, rawValue: number): any {
			//ѕроверка пассивок
			//todo...
			var skill = this.naked as IActiveNakedSkill;
			let value = new IntegerRef(rawValue);
			let dmg = new IntegerRef(0);

			switch (skill.type) {
				case ESkillType.physicalAttack:
				case ESkillType.physicalDamage:
					battle.do_event_actions("ДоНанесенияУронаПерсонажу", { target: to, initiator: from, supposed_damage: value, skill: this, damage_type: type });
					battle.do_event_actions("ДоНанесенияФизическогоУронаПерсонажу", { target: to, initiator: from, supposed_damage: value, skill: this, damage_type: type });
					dmg = new IntegerRef(value.value * (100 - to.ФизическаяЗащита.total_value) / 100);
					if(to.abilities_passive[35]) {
						to.abilities_passive[35].params.health -= dmg.value;
						battle.do_event_actions("ПослеНанесенияУронаПерсонажу", { target: to, initiator: from, damage: dmg, damage_type: type });
						battle.do_event_actions("ПослеНанесенияФизическогоУронаПерсонажу", { target: to, initiator: from, damage: dmg, damage_type: type });
					}
					break;
				case ESkillType.magicalAttack:
				case ESkillType.magicalDamage:
					battle.do_event_actions("ДоНанесенияУронаПерсонажу", { target: to, initiator: from, supposed_damage: value, skill: this, damage_type: type });
					battle.do_event_actions("ДоНанесенияМагическогоУронаПерсонажу", { target: to, initiator: from, supposed_damage: dmg, skill: this, damage_type: type });
					dmg = new IntegerRef(value.value * (100 - to.МагическаяЗащита.total_value) / 100);
					if(to.abilities_passive[35]) {
						to.abilities_passive[35].params.health -= dmg.value;
						battle.do_event_actions("ПослеНанесенияУронаПерсонажу", { target: to, initiator: from, damage: value, damage_type: type });
						battle.do_event_actions("ПослеНанесенияМагическогоУронаПерсонажу", { target: to, initiator: from, damage: dmg, damage_type: type });
					}
					break;
				case ESkillType.clearDamage:
					battle.do_event_actions("ПослеНанесенияУронаПерсонажу", { target: to, initiator: from, supposed_damage: value, skill: this, damage_type: type });
					battle.do_event_actions("ДоНанесенияЧистогоУронаПерсонажу", { target: to, initiator: from, supposed_damage: dmg, skill: this, damage_type: type });
					dmg = new IntegerRef(value.value);
					if(to.abilities_passive[35]) {
						to.abilities_passive[35].params.health -= dmg.value;
						battle.do_event_actions("ПослеНанесенияУронаПерсонажу", { target: to, initiator: from, damage: value, damage_type: type });
						battle.do_event_actions("ПослеНанесенияЧистогоУронаПерсонажу", { target: to, initiator: from, damage: dmg, damage_type: type });
					}
					break;
				default:
					break;
			}

			return { total_damage: dmg.value, success: true };
		};
	}
}