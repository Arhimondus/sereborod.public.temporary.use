/// <reference path="_Battle.ts" />
/// <reference path="_Hero.ts" />
/// <reference path="data/CheckDeath.ts" />
/// <reference path="data/Actives.ts" />
/// <reference path="data/Passives.ts" />
/// <reference path="Hero.ts" />
module Sereborod {
	export interface ISlot {
		id: number;
		targets: number[][]; //[[11,20,3],[32,4]]
	}
	export interface IBattleSkill {
		skill: HeroAbilityActive,
		targets: any; 
	}
	export class Battle {
		constructor(db_battle: DbBattle) {
			console.log("12345");
		};
		heroes: Sereborod.Hero[];
		get heroes_alive() {
			return this.heroes.filter(h => h.alive);
		};
		get heroes_not_alive() {
			return this.heroes.filter(h => !h.alive);
		};
		current_round: 0;
		to_log(message) {
			//LogMessages.Add(message);
		};

		decrease_time() {
			var GValue: any = {};
			for(var hero of this.heroes_alive) {
				for(var param of hero.params) {
					//var values = param.Value.filter(w => w.Type == GValue.ValueType.АктивнаяСпособность && w.Time >= 1);
					var values 
					for (var i = 0; i < values.Count; i++)
						values[ i ].Time--;
					values = param.value.filter(w => w.type == GValue.ValueType.АктивнаяСпособность && w.time == 0);
					for (var value of values)
						param.value.splice(param.value.findIndex(v => v == value));
						//param.value.Remove(value);
				}

				for (var skill of hero.abilities_active.filter(a => a.cooldown > 0)) {
					skill.cooldown_timer--;
				}

				for (var pa of hero.abilities_passive.filter(p => p.custom && p.d > 0)) {
					pa.d--;
				}
				hero.abilities_passive = hero.abilities_passive.filter(p => !(p.custom && p.d == 0));
			}
		};


		check_death() {
			var heroes_for_delete = this.heroes.filter(h => Sereborod.CheckDeath[h.death_type]());
			for(var hero of heroes_for_delete) {
				hero.slots = null;
				hero.alive = false;
				//Heroes.Remove(hero),
				//Notheroes_alive.Add(hero),
				//hero.Battle: null,
				this.to_log("Персонаж <span class='hero'>" + hero.name + "</span> скончался.");
			};
		};

		step: number = 1;

		/*IEnumerable<SkillData>*/ battle_skills: IBattleSkill[];
		do_all_actions() {
			for(var step = 0; step < 3; step++) {
				this.step = step;
				this.do_continue_action_skills();
				this.decrease_time();
				this.battle_skills = [...this.get_all_skills(step)];
				for(var skill of this.battle_skills.sort(o => o.skill.naked.priority).reverse()) {
					skill.skill.apply(this, skill.targets);
					skill.skill.cooldown_timer = skill.skill.cooldown;
				}
				this.check_death();
				if(this.check_end())
					return;
			}

			for(var hero of this.heroes_alive) {
				hero.energy += 3;
				if(hero.energy > hero.max_energy)
					hero.energy = hero.max_energy;
			}

			this.next_round();
			return this;
		};

		is_end: boolean;

		/*bool*/ check_end() {
			if(this.heroes_alive.length <= 1) {
				this.is_end = true;
				if(this.heroes_alive.length == 1) {
					var winSide = this.heroes.filter(h => h.side == this.heroes_alive[0].side);
					var loseSide = this.heroes.filter(h => h.side != this.heroes_alive[0].side);
					for(var hero of winSide) {
						DbHero.instant_add_experience(hero.id, 50);
						this.to_log("<span class='hero'>" + hero.name + "</span> +50 опыта");
					}
					for (var hero of loseSide) {
						DbHero.instant_add_experience(hero.id, 20);
						this.to_log("<span class='hero'>" + hero.name + "</span> +20 опыта");
					}
				}
				else {
					for (var hero of this.heroes) {
						DbHero.instant_add_experience(hero.id, 33);
						this.to_log("<span class='hero'>" + hero.name + "</span> +33 опыта");
					}
				}
				return true;
			}
			return false;
		};

		next_round() {
			for(var gHero of this.heroes_alive)
				gHero.slots = null;
			this.current_round++;
		};

		do_continue_action_skills() {
			for(var hero of this.heroes_alive)	{
				if(hero.Баффы.value.length > 0) {
					for(var value of hero.Баффы.value) {
						if(value.continue_action)
							value.skill.naked.continue_action(value.continue_data);
					}
				}
			}
		};


		//+/delegate void SkillAction (),

		/*+class SkillData
		{
			List<GHero> Targets,
			Skill Skill,
		}*/

		/*bool*/ check_targets(/*SkillData*/ sd) {
			if(sd.Targets != null) {
				for(var hero of sd.Targets) {
					if(hero == null)
						return false;
					if(!sd.Skill.Owner.Battle.Heroes.Contains(hero))
						return false;
					switch(sd.Skill.TargetType) {
						case ESkillTargetType.enemy:
							if (!sd.Skill.Owner.Battle.heroes_alive.filter(w => w.Side != sd.Skill.Owner.Side).Contains(hero))
								return false;
							break;
						case ESkillTargetType.ally:
							if (!sd.Skill.Owner.Battle.heroes_alive.filter(w => w.Side == sd.Skill.Owner.Side).Contains(hero))
								return false;
							break;
						case ESkillTargetType.ally_no_cur:
							if (!sd.Skill.Owner.Battle.heroes_alive.filter(w => w.Side == sd.Skill.Owner.Side).Contains(hero))
								return false;
							if (sd.Skill.Owner == hero)
								return false;
							break;
					}
				}
			}

			return true;
		};

		*get_all_skills(step) {
			for(var hero of this.heroes_alive) {
				if(hero.slots.length < 3) continue;

				var slot = hero.slots[step];
				if(slot == null || slot.id == 0) continue;
				var skill = hero.abilities_active.find(a => a.id == slot.id);

				if(skill.action_points == 2) {
					if (step == 2)
						continue;
					if (step == 1)
						hero.slots[2] = null;
					if (step == 0)
						hero.slots[1] = null;
				}

				if(skill.action_points == 3) {
					if(step == 0) {
						hero.slots[1] = null;
						hero.slots[2] = null;
					}
					else
						continue;
				}

				if(skill.mana_cost <= hero.energy)
					hero.energy -= skill.mana_cost;
				else
					continue;

				if(skill.cooldown_timer > 0)
					continue;

				if(skill != null) {
					var sd = { targets: slot.targets, skill: skill };
					if(this.check_targets(sd))
						yield sd;
				}
			}
		};

		do_event_actions(event_name, event_params: IEventParam) {
			for(var hero of this.heroes) {
				for(var pa of hero.abilities_passive.filter(p => p.naked.events.some(ev => ev == event_name))) {
					pa.apply(this, event_name, event_params);
				}
			}
		};
	}
}