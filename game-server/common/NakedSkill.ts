/// <reference path="Skill.ts" />
namespace Sereborod {
    export interface INakedSkill {
        id: number;
        name: string;
        description: string;
        params: number[][];
        priority?: number; //default = 1
        initialize?(skill: Skill, hero: Hero);
        continue_action?(data: any);
    }
    export interface IActiveNakedSkill extends INakedSkill {
        class: string,
        action_points: number[], //стоимость очков действия
        mana_cost: number[], //стоимость магической энергии
        cooldown: number[], //перезарядка в этапах
        step_duration?: number[], //длительность в этапах
        minimal_round?: number[], //минимальный раунд
        up_level: number[], //уровни для повышения
        type: ESkillType, //тип способности
        targets: ISkillTarget[], //список возможных типов целей
        apply(battle: Battle, targets: any, skill: Skill);
    }
    export interface IPassiveNakedSkill extends INakedSkill {
        events?: string[]; //список событий на которые срабатывает пассивная способность
        apply?(battle: Battle, eventType: string, eventParams: any, skill: Skill);
        //can_randomize: boolean;
    }
}