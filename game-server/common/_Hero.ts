namespace Sereborod {
    declare const db: any;
    export class DbHero {
        id: number;
        name: string;
        static async instant_add_experience(hero_id: number, experience: number) {
            var new_experience = await db.one('UPDATE heroes SET experience = experience + $<experience> WHERE id = $<hero_id> RETURNING experience', {
                experience,
                hero_id,
            });
            //Проверка таблички на уровни и т.д.
        }
    }
}