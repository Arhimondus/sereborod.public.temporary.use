namespace Sereborod {
    declare const db: any;
    export interface DbBattle {
        id: number,
        status: number,
        mode: string,
        params: any,
        heroes: DbBattle_Hero[],
    }

    export interface DbBattle_Hero {
        id: number,
        name: string,
    }
}