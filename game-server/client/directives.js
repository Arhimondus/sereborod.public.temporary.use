Vue.directive("hint", {
	inserted: function(el, bindings) {
		$inf = $(`<i title="${bindings.value}" class="hint fa fa-info-circle" aria-hidden="true"></i>`);
		$inf = $(el).prepend($inf).find('i.hint');
		$tooltip($inf);
	},
});

Vue.directive("error", {
	inserted: function(el, bindings) {
		var [param, message] = bindings.value;
	
		var def = $(el).closest("label").find("span").first().html();
		$(el).attr('data-default', def);
		
		if($(el).parent().prop("tagName") == "LABEL")
			$(el).attr('placeholder', def);
		$(el).attr('data-error-tag', "error_" + param);
		$(el).attr('data-error', message);

		// $(el).parent().append(`<div style="display: none;" class="error error_${param}"><span>${message}</span></div>`);
	},
	bind: function(el, bindings, vnode) {
		//el.addEventListener('keyup', function () {
			//$(this).parent().find(".error").hide();
			//$(this).parent().removeClass('error');
		//});
		el.addEventListener('blur', function () {
			var def = $(this).attr('data-default');
			if($(el).parent().prop("tagName") == "LABEL")
				$(this).attr('placeholder', def);
			$(this).closest("label").find('span').first().text(def);
			$(this).closest("label").removeClass('error');
		});
	},
});

Vue.directive("scrollable", {
	inserted: function(el, bindings) {
		//var [param, message] = eval(bindings.expression);
		$def = $(el).wrap('<div class="scrollable"></div>');
	},
});

Vue.directive("r", {
	inserted: function(el, bindings) {
		$(el).addClass("required");
	},
});

Vue.directive("disabled", {
	inserted: function(el, bindings) {
		var result = bindings.value;
		if(result) {
			$(el).addClass("disabled");
			$(el).find("input").attr("disabled", "disabled");
			$(el).find("select").attr("disabled", "disabled");
		}
	},
	update: function (el, bindings) {
		var result = bindings.value;
		if(result) {
			$(el).addClass("disabled");
			$(el).find("input").attr("disabled", "disabled");
			$(el).find("select").attr("disabled", "disabled");
		} else {
			$(el).removeClass("disabled");
			$(el).find("input").removeAttr("disabled");
			$(el).find("select").removeAttr("disabled", "disabled");
		}
	},
});

Vue.directive("allow-edit", {
	inserted: function(el, bindings) {
		var roles = eval(bindings.expression).split(',');
		
		var disable = true;
		if (typeof(roles) == 'object') {
			for (var i = 0; i < roles.length; ++i) {
				if (roles[i] == store.get("role")) {
					disable = false;
					break;
				}
			}
		}
	
		if (disable) {
			$(el).addClass("disabled");
			$(el).find("input").attr("disabled", "disabled");
		}
	},
});