function preloadRoutes(routes) {
	var current = 0;
	var children = [];
	for(var i = 0; i < routes.length; i++) {
		var route = routes[i];
		
		if(route.path.length <= 2) continue;
		
		var controller = route.controller || {};
		var template = route.template || route.controller;
				
		var component = CONTROLLERS[controller + ".js"];
		component.template = TEMPLATES[template + ".html"];

		component.beforeRouteEnter = (to, from, next) => {		
			if(to.meta.access == null) return next();
			
			let session = typeof($app) != "undefined" ? $app.session : store.get("session");
			let role = typeof($app) != "undefined" ? $app.role : store.get("role");
			
			if(role == null || session == null) 
				next({ path: '/login' });
			else {
				if(to.meta.access.length != 0 && to.meta.access.indexOf(role) < 0)
					next({ path: '/denied' });
				else {
					next();
				}
			}
		};
		route.component = component;
		children.push(route);
	}
	children.push({ path: '/', redirect: "/index", meta: { access: null, tabbed: false } });
	return children;
}
	
let routes = preloadRoutes([	
	{ path: '/index', controller: "index", meta: { tabbed: false, access: null } },
	{ path: '/login', controller: "login", meta: { tabbed: false, access: null } },
	{ path: '/logout', controller: "logout", meta: { tabbed: false, access: null } },
	{ path: '/register', controller: "register", meta: { tabbed: false, access: null } },
	{ path: '/lobby', controller: "lobby", meta: { tabbed: false, access: null } },
	{ path: '/hero/list', controller: "hero_list", meta: { tabbed: false, access: null } },
	{ path: '/hero/create', controller: "hero_create", meta: { tabbed: false, access: null } },
	{ path: '/hero/view', controller: "hero_view", meta: { tabbed: false, access: null } },
	{ path: '/game', controller: "game", meta: { tabbed: false, access: null } },
]);

$router = new VueRouter({
	//mode: 'history',
	routes
});
		
$router.afterEach(() => {
	$("html, body").animate({ scrollTop: 0 }, "fast");
});