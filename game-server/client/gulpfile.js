﻿var gulp = require('gulp'),
	concat = require('gulp-concat'),
	html2string = require('gulp-html2string'),
	htmlMinifier = require('gulp-html-minifier'),
	rename = require('gulp-rename'),
	insert = require('gulp-insert'),
	bom = require('gulp-bom'),
	path = require('path'),
	sass = require('gulp-sass'),
	babel = require('gulp-babel'),
	ts = require('gulp-typescript'),
	runSequence = require('run-sequence'),
	replace = require('gulp-replace'),
	fs = require('fs'),
	argv = require('yargs').argv;

gulp.task('routes', function() {
	return gulp.src('pages/*.js')
	.pipe(insert.transform(function(contents, file) {
		return "CONTROLLERS['" + path.basename(file.path) + "'] = " + contents + ";";
	}))
	.pipe(concat('controllers.js'))
	.pipe(insert.prepend('CONTROLLERS = {};\r\n'))
	.pipe(gulp.dest(''));
});

gulp.task('templates', function() {
	return gulp.src('pages/*.html')
	.pipe(html2string({
		base: 'pages',
		createObj: false,
		objName: 'TEMPLATES'
	}))
	.pipe(concat('templates.js'))
	.pipe(insert.prepend('TEMPLATES = {};\r\n'))
	.pipe(gulp.dest(''));
});

gulp.task('components', function() {
	return gulp.src('components/*.js')
	.pipe(concat('components.js'))
	.pipe(gulp.dest(''));
});

gulp.task('directives', function() {
	return gulp.src('directives/*.js')
	.pipe(concat('directives.js'))
	.pipe(gulp.dest(''));
});

gulp.task('sass', function() {
	return gulp.src('styles.scss')
	.pipe(sass.sync().on('error', sass.logError))
	.pipe(gulp.dest(''));
});

gulp.task('watch', ['routes', 'components', 'templates', 'directives', 'sass'], function() {
	gulp.watch('pages/*.js', ['routes']);
	gulp.watch('components/*.js', ['components']);
	gulp.watch('pages/*.html', ['templates']);
	gulp.watch('pages/*.html', ['directives']);
	gulp.watch('styles.scss', ['sass']);
});	

gulp.task('babel', function() {
	return gulp.src([
		'polyfills.js',
		'api.js',
		'misc.js',
		argv.debugScript,
		'controllers.js',
		'templates.js',
		'routes.js',
		'components.js',
		'application.js',		
	])
	.pipe(babel({
		presets: ['es2015'],
		plugins: ['transform-remove-strict-mode'],
	}))
	.pipe(concat("app.js"))
	.pipe(replace(/API = "(.*?)"/g, `API = "api_service/v1"`))
	.pipe(gulp.dest(argv.destFolder));
});

gulp.task('version_replace', function() {
	return gulp.src('index.html')
	.pipe(replace(/%%([0-9]*)%%/g, '%%' + Date.now() + '%%'))
	.pipe(gulp.dest(''));
});

gulp.task('media', function() {
	return gulp.src([
		'media/**/*',
	], { "base" : "." })
	.pipe(gulp.dest(argv.destFolder));
});

gulp.task('libs', function() {
	return gulp.src([
		'libs/jquery.min.js',
		'libs/vue.min.js',
		'libs/vue-router.min.js',
		'libs/store.min.js',
		'libs/notify.min.js',
	])
	.pipe(concat("libs.js"))
	.pipe(gulp.dest(argv.destFolder));
});



gulp.task('default', function(done) {
	runSequence('sass', 'ts', done);
});