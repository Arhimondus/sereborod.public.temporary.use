TEMPLATES = {};
TEMPLATES['game.html'] = "<main class=\"game mode-default\">\n" +
    "	<h2>Команда 1</h2>\n" +
    "	<ul v-for=\"hero in teamOne\">\n" +
    "		<li>\n" +
    "			<span>{{ hero.name }}</span>\n" +
    "		</li>\n" +
    "	</ul>\n" +
    "	<h2>Команда 2</h2>\n" +
    "	<ul v-for=\"\">\n" +
    "		<li></li>\n" +
    "	</ul>\n" +
    "</main>\n" +
    ""; 
TEMPLATES['hero_create.html'] = "<main class=\"centered\">\n" +
    "	<form ref=\"form\" method=\"post\" action=\"/hero/create\" v-on:submit.prevent>\n" +
    "		<label>\n" +
    "			<span>Имя героя</span>\n" +
    "			<input type=\"text\" v-model=\"form.name\"/>\n" +
    "		</label>\n" +
    "		<label>\n" +
    "			<span>Класс</span>\n" +
    "			<select v-model=\"form.class\">\n" +
    "				<option value=\"gladiator\">Гладиатор</option>\n" +
    "				<option value=\"maroder\">Мародёр</option>\n" +
    "				<option value=\"fanatik\">Фанатик</option>\n" +
    "			</select>\n" +
    "		</label>\n" +
    "		<label>\n" +
    "			<input @click=\"create\" type=\"submit\" value=\"Создать\"/>\n" +
    "		</label>\n" +
    "	</form>\n" +
    "</main>"; 
TEMPLATES['hero_list.html'] = "<main class=\"centered\">\n" +
    "	<form class=\"centered\">\n" +
    "		<p>Добро пожаловать, <span class=\"nowrap\">{{ $root.name }}</span></p>\n" +
    "		<router-link to=\"/logout\">Выход</router-link>\n" +
    "		<ul>\n" +
    "			<li v-for=\"hero in heroes\">\n" +
    "				<a @click=\"select(hero)\" href=\"#\">{{ hero.name }} {{ hero.class }} [{{ hero.level }}]</a>\n" +
    "			</li>\n" +
    "		</ul>\n" +
    "		<div>Всего {{ heroes.length }} / {{ $root.heroes_limit }}</div>\n" +
    "		<router-link v-if=\"heroes.length < $root.heroes_limit\" to=\"/hero/create\">Создать героя</router-link>\n" +
    "		<p v-else>Больше героев<br/>создавать нельзя!</p>		\n" +
    "	</form>\n" +
    "</main>\n" +
    ""; 
TEMPLATES['hero_view.html'] = "<main class=\"centered\">\n" +
    "	<form class=\"centered\">\n" +
    "		<div>{{ name }}</div>\n" +
    "		<div>Всего {{ heroes.length }} / {{ $root.heroes_limit }}</div>\n" +
    "		<router-link v-if=\"heroes.length < $root.heroes_limit\" to=\"/hero/create\">Создать героя</router-link>\n" +
    "		<p v-else>Больше героев<br/>создавать нельзя!</p>\n" +
    "	</form>\n" +
    "</main>\n" +
    ""; 
TEMPLATES['index.html'] = "<main class=\"centered\">\n" +
    "	<router-link to=\"/register\">Регистрация</router-link>\n" +
    "	<router-link to=\"/login\">Логин</router-link>\n" +
    "</main>\n" +
    ""; 
TEMPLATES['lobby.html'] = "<main class=\"lobby\">\n" +
    "	<ul v-for=\"battle in open_battles\">\n" +
    "		<li>\n" +
    "			<header>Битва №{{ battle.id }}</header>\n" +
    "			<div class=\"side one\">\n" +
    "				{{ battle.heroes | heroes }}\n" +
    "				<button v-if=\"$root.battle == null\" @click=\"join(battle.id)\">Присоединиться</button>\n" +
    "				<button v-if=\"$root.battle && $root.battle.status == 1 && $root.battle.id == battle.id\" @click=\"leave(battle.id)\">Покинуть</button>\n" +
    "			</div>\n" +
    "		</li>\n" +
    "	</ul>\n" +
    "	<div>\n" +
    "		<button v-if=\"$root.battle == null\" @click=\"create\">Создать</button>\n" +
    "	</div>\n" +
    "</main>\n" +
    ""; 
TEMPLATES['login.html'] = "<main class=\"centered\">\n" +
    "	<form v-on:submit.prevent>\n" +
    "		<label>Логин:<input type=\"text\" v-model=\"form.login\"/></label>\n" +
    "		<label>Пароль:<input type=\"password\" v-model=\"form.password\"/></label>\n" +
    "		<label><input @click=\"submit\" type=\"submit\" value=\"Вход\"/></label>\n" +
    "		<label><router-link to=\"/register \">Регистрация</router-link></label>\n" +
    "	</form>\n" +
    "</main>\n" +
    ""; 
TEMPLATES['logout.html'] = "<div>Производится выход</div>"; 
TEMPLATES['register.html'] = "<form ref=\"form\" method=\"post\" action=\"/register\" v-on:submit.prevent>\n" +
    "	<label>Идентификатор сессии:<input style=\"background-color: lightgray; border: 0;\" readonly type=\"text\" name=\"identifier\"/></label>\n" +
    "	<label>Имя:<input type=\"text\" name=\"name\"/></label>\n" +
    "	<label>Логин:<input type=\"text\" name=\"login\"/></label>\n" +
    "	<label>Почта:<input type=\"text\" name=\"email\"/></label>\n" +
    "	<label>Пароль:<input type=\"password\" name=\"password\"/></label>\n" +
    "	<label><img id=\"captcha\" src=\"\"/>Капча:<input type=\"text\" name=\"token\"/></label>\n" +
    "	<label><input @click=\"submit\" type=\"submit\" value=\"Регистрация\"/></label>\n" +
    "</form>"; 
TEMPLATES['settings.html'] = "<section>\n" +
    "	<router-link to=\"/hero/list\">Сменить героя</router-link>\n" +
    "	<router-link to=\"/logout\">Выход</router-link>\n" +
    "</section>\n" +
    ""; 