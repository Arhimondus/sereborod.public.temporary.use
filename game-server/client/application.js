﻿$app = new Vue({
	data: { 
		uid: store.get("uid"),
		name: store.get("name") || "Гость",
		token: store.get("token"),
		heroes_limit: store.get("heroes_limit") || 1,
		hero: store.get("hero"),
		battle: null,
	},
	computed: {
		title() {
			if(this.$route && this.$route.matched.length > 0)
				return this.$route.matched[0].components.default.title;
			else
				return "Неопознанная страница";
		},
		is_user() {
			return this.token != null;
		},
	},
	methods: {
		logout() {
			this.$router.replace({ path: '/logout'});
		},
		set_tabs() {
			
		},
	},
	watch: {
		$route: 'set_tabs',
		uid(value) {
			store.set("uid", value);
		},
		name(value) {
			store.set("name", value);
		},
		token(value) {
			store.set("token", value);
		},
		heroes_limit(value) {
			store.set("heroes_limit", value);
		},
		hero(value) {
			store.set("hero", value);
		},
		/*["hero.battle"](value) {
			store.set("hero", this.hero);
		},*/
	},
	created() {
		
	},
	mounted() {
		
	},
	router: $router,
});