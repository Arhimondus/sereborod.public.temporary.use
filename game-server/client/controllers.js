CONTROLLERS = {};
CONTROLLERS['game.js'] = {
	title: "Игра",
	data() {
		return {
			socket: io("/", { query: "token=" + $app.token }),
			battle: null,
			teams: null,
		};
	},
	methods: {
		get_all() {
			this.socket.emit('game/data', (data) => {
				this.data = data;
			});
		},
	},
	mounted() {
		this.get_all();
		this.socket.on('next/round', (battle) => {
			this.get_all();
		});
	},
};
CONTROLLERS['hero_create.js'] = {
	title: "Создать нового героя",
	data() {
		return {
			form: {
				name: "",
				class: ""
			},
		};
	},
	methods: {
		create() {
			$.post("/hero/create?token=" + this.$root.token, $.extend({}, this.form), () => {
				this.$router.push("/hero/list");
			}, 'json').fail((error) => {
				alert(error);
			});		
		},
	},
};
CONTROLLERS['hero_list.js'] = {
	title: "Выбор героя",
	data() {
		return {
			heroes: [],
		};
	},
	filters: {
		
	},
	methods: {
		select(hero) {
			$.post("/hero/select?token=" + this.$root.token, { id: hero.id }, (hero) => {
				this.$root.hero = hero;
				this.$router.push("/lobby");
			});		
		},
	},
	mounted() {
		$.getJSON("/hero/list?token=" + this.$root.token, (heroes) => {
			this.heroes = heroes;
		});		
	},
};
CONTROLLERS['hero_view.js'] = {
	title: "Выбор героя",
	data() {
		return {
			hero: null,
		};
	},
	filters: {
		
	},
	methods: {

	},
	mounted() {
		$.getJSON("/hero/view?token=" + this.$root.token, (hero) => {
			this.hero = hero;
		});		
	},
};
CONTROLLERS['index.js'] = {
	methods: {
	},
	mounted() {
		if(this.$root.hero != null) return this.$router.push("/lobby");
		else return this.$router.push("/login");
	},
};
CONTROLLERS['lobby.js'] = {
	title: "Лобби",
	data() {
		return {
			socket: io("/", { query: "token=" + $app.token }),
			open_battles: [],
		};
	},
	filters: {
		heroes(data) {
			return data.map((m) => JSON.stringify(m)).reduce((a, b) => a + ", " + b);
		}
	},
	methods: {
		join(battle_id) {
			this.socket.emit('battle/join', battle_id);
			//wait for battle/join/success
		},
		create() {
			this.socket.emit('battle/create', {
				params: {},
				mode: "default",
			});
		},
		leave(battle_id) {
			this.socket.emit('battle/leave', battle_id);
			//wait for battle/leave/success
		},
		get_all() {
			$.getJSON("/battle/status/open", (battles) => {
				this.open_battles = battles;
			});
			this.socket.emit('hero/current_battle', (battle) => {
				this.$root.battle = battle;
			});
		},
	},
	mounted() {
		if(this.$root.battle != null && this.$root.battle.status == 2) return this.$router.push("/game");
		this.get_all();
		this.socket.on('battle/create', (battle) => {
			this.open_battles.push(battle);
		});
		this.socket.on('battle/change', (battle) => {
			if(battle.id == this.$root.hero.battle && battle.status == 2) return this.$router.push("/game");
			var index = this.open_battles.findIndex((b) => b.id == battle.id);
			if(index >= 0)
				this.open_battles[index] = battle;
			else
				this.open_battles.push(battle);
		});
		this.socket.on('battle/join/success', (battle) => {
			this.$root.hero.battle = battle;
			if(battle.status == 2) return this.$router.push("/game");
			this.open_battles.find((b) => b.id == battle.id).heroes.push({ id: this.$root.hero.id, name: this.$root.hero.name, });
		});
		this.socket.on('battle/leave/success', (battle) => {
			this.$root.battle = null;
			this.get_all();
		});
		this.socket.on('battle/create/success', (battle) => {
			this.$root.battle = battle;
			this.open_battles.push(battle);
		});
		this.socket.on('battle/remove', (battle) => {
			this.open_battles.splice(this.open_battles.findIndex(b => b.id == battle.id), 1);
		});
	},
};
CONTROLLERS['login.js'] = {
	title: "Вход",
	data() {
		return {
			form: {
				login: "",
				password: "",
			},
		};
	},
	methods: {
		submit() {
			$.post("/login", $.extend({}, this.form), (data) => {
				this.$root.uid = data.uid;
				this.$root.name = data.name;
				this.$root.token = data.token;
				this.$root.heroes_limit = data.heroes_limit;
				this.$router.push("/hero/list");
			}).fail(({ responseText }) => {
				alert(responseText);
			});
		},
	},
	mounted() {
	},
};
CONTROLLERS['logout.js'] = {
	mounted() {
		this.$root.uid = null;
		this.$root.name = 'Гость';
		this.$root.token = null;
		this.$root.heroes_limit = 1;
		this.$router.push("login");
	},
};
CONTROLLERS['register.js'] = {
	methods: {
		submit() {
			var form = this.$refs.form;
			$.post($(form).attr("action"), $(form).serialize(), (data) => {
				this.$root.uid = data.uid;
				this.$root.name = data.name;
				this.$root.token = data.token;
				this.$router.push("hero/list");
			}).fail(data => {
				alert(JSON.stringify(data));
				this.get_captcha();
			});
		},
		get_captcha() {
			return new Promise((resolve, reject) => {
				$.getJSON("/register/captcha", function(res) {
					$("#captcha").attr("src", "data:image/png;base64," + res.captcha);
					$("[name=identifier]").val(res.identifier);
					resolve();
				}).fail(() => {
					reject();
				});
			});
		},
	}, 
	mounted: async function() {
		await this.get_captcha();
	},
};
CONTROLLERS['settings.js'] = {
	methods: {
	},
	mounted() {
	},
};