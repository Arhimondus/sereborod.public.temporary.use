{
	title: "Игра",
	data() {
		return {
			socket: io("/", { query: "token=" + $app.token }),
			battle: null,
			teams: null,
		};
	},
	methods: {
		get_all() {
			this.socket.emit('game/data', (data) => {
				this.data = data;
			});
		},
	},
	mounted() {
		this.get_all();
		this.socket.on('next/round', (battle) => {
			this.get_all();
		});
	},
}