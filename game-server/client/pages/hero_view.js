{
	title: "Выбор героя",
	data() {
		return {
			hero: null,
		};
	},
	filters: {
		
	},
	methods: {

	},
	mounted() {
		$.getJSON("/hero/view?token=" + this.$root.token, (hero) => {
			this.hero = hero;
		});		
	},
}