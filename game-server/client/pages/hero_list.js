{
	title: "Выбор героя",
	data() {
		return {
			heroes: [],
		};
	},
	filters: {
		
	},
	methods: {
		select(hero) {
			$.post("/hero/select?token=" + this.$root.token, { id: hero.id }, (hero) => {
				this.$root.hero = hero;
				this.$router.push("/lobby");
			});		
		},
	},
	mounted() {
		$.getJSON("/hero/list?token=" + this.$root.token, (heroes) => {
			this.heroes = heroes;
		});		
	},
}