{
	title: "Лобби",
	data() {
		return {
			socket: io("/", { query: "token=" + $app.token }),
			open_battles: [],
		};
	},
	filters: {
		heroes(data) {
			return data.map((m) => JSON.stringify(m)).reduce((a, b) => a + ", " + b);
		}
	},
	methods: {
		join(battle_id) {
			this.socket.emit('battle/join', battle_id);
			//wait for battle/join/success
		},
		create() {
			this.socket.emit('battle/create', {
				params: {},
				mode: "default",
			});
		},
		leave(battle_id) {
			this.socket.emit('battle/leave', battle_id);
			//wait for battle/leave/success
		},
		get_all() {
			$.getJSON("/battle/status/open", (battles) => {
				this.open_battles = battles;
			});
			this.socket.emit('hero/current_battle', (battle) => {
				this.$root.battle = battle;
			});
		},
	},
	mounted() {
		if(this.$root.battle != null && this.$root.battle.status == 2) return this.$router.push("/game");
		this.get_all();
		this.socket.on('battle/create', (battle) => {
			this.open_battles.push(battle);
		});
		this.socket.on('battle/change', (battle) => {
			if(battle.id == this.$root.hero.battle && battle.status == 2) return this.$router.push("/game");
			var index = this.open_battles.findIndex((b) => b.id == battle.id);
			if(index >= 0)
				this.open_battles[index] = battle;
			else
				this.open_battles.push(battle);
		});
		this.socket.on('battle/join/success', (battle) => {
			this.$root.hero.battle = battle;
			if(battle.status == 2) return this.$router.push("/game");
			this.open_battles.find((b) => b.id == battle.id).heroes.push({ id: this.$root.hero.id, name: this.$root.hero.name, });
		});
		this.socket.on('battle/leave/success', (battle) => {
			this.$root.battle = null;
			this.get_all();
		});
		this.socket.on('battle/create/success', (battle) => {
			this.$root.battle = battle;
			this.open_battles.push(battle);
		});
		this.socket.on('battle/remove', (battle) => {
			this.open_battles.splice(this.open_battles.findIndex(b => b.id == battle.id), 1);
		});
	},
}