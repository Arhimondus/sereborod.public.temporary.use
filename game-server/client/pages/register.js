{
	methods: {
		submit() {
			var form = this.$refs.form;
			$.post($(form).attr("action"), $(form).serialize(), (data) => {
				this.$root.uid = data.uid;
				this.$root.name = data.name;
				this.$root.token = data.token;
				this.$router.push("hero/list");
			}).fail(data => {
				alert(JSON.stringify(data));
				this.get_captcha();
			});
		},
		get_captcha() {
			return new Promise((resolve, reject) => {
				$.getJSON("/register/captcha", function(res) {
					$("#captcha").attr("src", "data:image/png;base64," + res.captcha);
					$("[name=identifier]").val(res.identifier);
					resolve();
				}).fail(() => {
					reject();
				});
			});
		},
	}, 
	mounted: async function() {
		await this.get_captcha();
	},
}