{
	title: "Вход",
	data() {
		return {
			form: {
				login: "",
				password: "",
			},
		};
	},
	methods: {
		submit() {
			$.post("/login", $.extend({}, this.form), (data) => {
				this.$root.uid = data.uid;
				this.$root.name = data.name;
				this.$root.token = data.token;
				this.$root.heroes_limit = data.heroes_limit;
				this.$router.push("/hero/list");
			}).fail(({ responseText }) => {
				alert(responseText);
			});
		},
	},
	mounted() {
	},
}