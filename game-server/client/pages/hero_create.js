{
	title: "Создать нового героя",
	data() {
		return {
			form: {
				name: "",
				class: ""
			},
		};
	},
	methods: {
		create() {
			$.post("/hero/create?token=" + this.$root.token, $.extend({}, this.form), () => {
				this.$router.push("/hero/list");
			}, 'json').fail((error) => {
				alert(error);
			});		
		},
	},
}