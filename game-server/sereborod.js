var pgp = require('pg-promise')();
	//var cfg = require('./server.cfg.js');
	//var db = pgp(cfg.db);
	var Sereborod;
	(function (Sereborod) {
	class DbHero {
		static async instant_add_experience(hero_id, experience) {
			var new_experience = await db.one('UPDATE heroes SET experience = experience + $<experience> WHERE id = $<hero_id> RETURNING experience', {
				experience,
				hero_id,
			});
		}
	}
	Sereborod.DbHero = DbHero;

	function D3(numbers) {
		return numbers.map(m => m * 3);
	}
	Sereborod.D3 = D3;
	class IntegerRef {
		constructor(value) {
			this.value = value;
		}
	}
	var ESkillType;
	(function (ESkillType) {
		ESkillType[ESkillType["physicalSkill"] = 1] = "physicalSkill";
		ESkillType[ESkillType["physicalAttack"] = 2] = "physicalAttack";
		ESkillType[ESkillType["physicalDamage"] = 3] = "physicalDamage";
		ESkillType[ESkillType["magicalSkill"] = 4] = "magicalSkill";
		ESkillType[ESkillType["magicalAttack"] = 5] = "magicalAttack";
		ESkillType[ESkillType["magicalDamage"] = 6] = "magicalDamage";
		ESkillType[ESkillType["clearDamage"] = 7] = "clearDamage";
		ESkillType[ESkillType["mentalAttack"] = 8] = "mentalAttack";
		ESkillType[ESkillType["mentalDamage"] = 9] = "mentalDamage";
		ESkillType[ESkillType["mentalSkill"] = 10] = "mentalSkill";
		ESkillType[ESkillType["mentalPurge"] = 11] = "mentalPurge";
	})(ESkillType = Sereborod.ESkillType || (Sereborod.ESkillType = {}));

	var ESkillTargetType;
	(function (ESkillTargetType) {
		ESkillTargetType[ESkillTargetType["no_target"] = 0] = "no_target";
		ESkillTargetType[ESkillTargetType["any_unit"] = 1] = "any_unit";
		ESkillTargetType[ESkillTargetType["ally"] = 2] = "ally";
		ESkillTargetType[ESkillTargetType["enemy"] = 3] = "enemy";
		ESkillTargetType[ESkillTargetType["neutral"] = 4] = "neutral";
		ESkillTargetType[ESkillTargetType["ally_no_cur"] = 5] = "ally_no_cur";
	})(ESkillTargetType = Sereborod.ESkillTargetType || (Sereborod.ESkillTargetType = {}));
	class Skill {
		constructor(skill_id, owner, params) {
			this.cooldown_timer = 0;
		}

		getHeroParam(type) {
			return this.owner.params.find(p => p.type == type).total_value;
		}

		byLevelParam(number = 1, based_word = null) {
			var calculated = this.naked.params[number][this.level - 1];
			if (based_word != null) {
				var item = based_word.find((i) => i[0] == calculated);
				if (item != null)
					calculated += item[1];
			}
			return calculated;
		}

		restoreHealth(initiator, target, skill_type, value) {
			return 10;
		}

		baseOnParam(param_number, range_numbers) {
			let modifier = 0;
			for (var [check, set] of range_numbers.reverse()) {
				if (param_number >= check) {
					modifier = set;
					break;
				}
			}
			return modifier;
		}

		random(number = 101) {
			return this.random_f() * 100;
		}

		random_f(number = 1.1) {
			return Math.random();
		}

		makeDamage(battle, from, to, type, rawValue) {
			var skill = this.naked;
			let value = new IntegerRef(rawValue);
			let dmg = new IntegerRef(0);
			switch (skill.type) {
				case ESkillType.physicalAttack:
				case ESkillType.physicalDamage:
					battle.do_event_actions("ДоНанесенияУронаПерсонажу", { target: to, initiator: from, supposed_damage: value, skill: this, damage_type: type });
					battle.do_event_actions("ДоНанесенияФизическогоУронаПерсонажу", { target: to, initiator: from, supposed_damage: value, skill: this, damage_type: type });
					dmg = new IntegerRef(value.value * (100 - to.ФизическаяЗащита.total_value) / 100);
					if (to.abilities_passive[35]) {
						to.abilities_passive[35].params.health -= dmg.value;
						battle.do_event_actions("ПослеНанесенияУронаПерсонажу", { target: to, initiator: from, damage: dmg, damage_type: type });
						battle.do_event_actions("ПослеНанесенияФизическогоУронаПерсонажу", { target: to, initiator: from, damage: dmg, damage_type: type });
					}
					break;
				case ESkillType.magicalAttack:
				case ESkillType.magicalDamage:
					battle.do_event_actions("ДоНанесенияУронаПерсонажу", { target: to, initiator: from, supposed_damage: value, skill: this, damage_type: type });
					battle.do_event_actions("ДоНанесенияМагическогоУронаПерсонажу", { target: to, initiator: from, supposed_damage: dmg, skill: this, damage_type: type });
					dmg = new IntegerRef(value.value * (100 - to.МагическаяЗащита.total_value) / 100);
					if (to.abilities_passive[35]) {
						to.abilities_passive[35].params.health -= dmg.value;
						battle.do_event_actions("ПослеНанесенияУронаПерсонажу", { target: to, initiator: from, damage: value, damage_type: type });
						battle.do_event_actions("ПослеНанесенияМагическогоУронаПерсонажу", { target: to, initiator: from, damage: dmg, damage_type: type });
					}
					break;
				case ESkillType.clearDamage:
					battle.do_event_actions("ПослеНанесенияУронаПерсонажу", { target: to, initiator: from, supposed_damage: value, skill: this, damage_type: type });
					battle.do_event_actions("ДоНанесенияЧистогоУронаПерсонажу", { target: to, initiator: from, supposed_damage: dmg, skill: this, damage_type: type });
					dmg = new IntegerRef(value.value);
					if (to.abilities_passive[35]) {
						to.abilities_passive[35].params.health -= dmg.value;
						battle.do_event_actions("ПослеНанесенияУронаПерсонажу", { target: to, initiator: from, damage: value, damage_type: type });
						battle.do_event_actions("ПослеНанесенияЧистогоУронаПерсонажу", { target: to, initiator: from, damage: dmg, damage_type: type });
					}
					break;
				default:
					break;
			}
			return { total_damage: dmg.value, success: true };
		}

	}
	Sereborod.Skill = Skill;

	var EParamType;
	(function (EParamType) {
		EParamType[EParamType["Сила"] = 0] = "Сила";
		EParamType[EParamType["Ловкость"] = 1] = "Ловкость";
		EParamType[EParamType["Интеллект"] = 2] = "Интеллект";
		EParamType[EParamType["Выносливость"] = 3] = "Выносливость";
		EParamType[EParamType["Удача"] = 4] = "Удача";
		EParamType[EParamType["ФизическаяЗащита"] = 5] = "ФизическаяЗащита";
		EParamType[EParamType["МагическаяЗащита"] = 6] = "МагическаяЗащита";
		EParamType[EParamType["ПовышениеУрона"] = 7] = "ПовышениеУрона";
		EParamType[EParamType["ПовышениеЗащиты"] = 8] = "ПовышениеЗащиты";
		EParamType[EParamType["Баффы"] = 9] = "Баффы";
	})(EParamType = Sereborod.EParamType || (Sereborod.EParamType = {}));
	class Param {
		constructor(type = null, initial = null) {
			if (type != null)
				this.type = type;
			if (initial != null)
				this.value.push(initial);
		}
		get total_value() {
			var absoluteValues = this.value.filter(v => v.number_type == ValueNumberType.Абсолютное).map(v => v.value);
			var av = 0;
			if (absoluteValues.length > 0)
				av = absoluteValues.reduce((prev, curr) => prev + curr);
			var percentageValues = this.value.filter(v => v.number_type == ValueNumberType.Процентное).map(v => v.value);
			var pv = 0;
			if (percentageValues.length > 0)
				pv = percentageValues.reduce((prev, curr) => prev + curr);
			av = av + av * pv / 100;
			return av;
		}
		get hard_value() {
			var vs = this.value.find(f => f.type == ValueType.Параметр);
			return vs != null ? vs.value : 0;
		}
		set hard_value(value) {
			var vs = this.value.find(f => f.type == ValueType.Параметр);
			vs.value = value;
		}
		get items_value() {
			var vs = this.value.find(f => f.type == ValueType.Предмет);
			return vs != null ? vs.value : 0;
		}
		get ability_value() {
			var vs = this.value.find(f => f.type == ValueType.ПассивнаяСпособность);
			return vs != null ? vs.value : 0;
		}
		get permanent_value() {
			return this.hard_value + this.items_value + this.ability_value;
		}
		get float_value() {
			var vs = this.value.find(f => f.type == ValueType.АктивнаяСпособность);
			return vs != null ? vs.value : 0;
		}
	}
	Sereborod.Param = Param;
	var ValueType;
	(function (ValueType) {
		ValueType[ValueType["Параметр"] = 0] = "Параметр";
		ValueType[ValueType["Предмет"] = 1] = "Предмет";
		ValueType[ValueType["АктивнаяСпособность"] = 2] = "АктивнаяСпособность";
		ValueType[ValueType["ПассивнаяСпособность"] = 3] = "ПассивнаяСпособность";
	})(ValueType || (ValueType = {}));

	var ValueNumberType;
	(function (ValueNumberType) {
		ValueNumberType[ValueNumberType["Абсолютное"] = 0] = "Абсолютное";
		ValueNumberType[ValueNumberType["Процентное"] = 1] = "Процентное";
	})(ValueNumberType || (ValueNumberType = {}));

	class Value {
		constructor() {
			this.time = -1;
		}
	}
	class HeroType {
	}
	class HeroParams {
	}
	Sereborod.HeroParams = HeroParams;
	class HeroAbilityActive extends Skill {
		get action_points() {
			return this.naked.action_points[this.level];
		}

		get mana_cost() {
			return this.naked.mana_cost[this.level];
		}

		get cooldown() {
			return this.naked.cooldown[this.level];
		}

		get step_duration() {
			return this.naked.step_duration[this.level];
		}

		get minimal_round() {
			return this.naked.minimal_round[this.level];
		}

		get up_level() {
			return this.naked.up_level[this.level];
		}

		apply(battle, targets) {
			this.naked.apply(battle, targets, this);
		}

	}
	Sereborod.HeroAbilityActive = HeroAbilityActive;
	class HeroAbilityPassive extends Skill {
		constructor(skill_id, owner, params) {
			super(skill_id, owner, params);
			this.naked = Sereborod.Passives[skill_id];
		}

		apply(battle, eventType, eventParams) {
			this.naked.apply(battle, eventType, eventParams, this);
		}

	}
	Sereborod.HeroAbilityPassive = HeroAbilityPassive;
	class Hero extends HeroParams {
		preInitHero() {
			this.abilities_active
				.filter(a => a.naked.initialize)
				.forEach((a) => a.naked.initialize(a, this));
			this.abilities_passive
				.filter(a => a.naked.initialize)
				.forEach((p) => p.naked.initialize(p, this));
		}

		get Сила() {
			return this.params.find(f => f.type == EParamType.Сила);
		}

		get Ловкость() {
			return this.params.find(f => f.type == EParamType.Ловкость);
		}

		get Интеллект() {
			return this.params.find(f => f.type == EParamType.Интеллект);
		}

		get Удача() {
			return this.params.find(f => f.type == EParamType.Удача);
		}

		get Выносливость() {
			return this.params.find(f => f.type == EParamType.Выносливость);
		}

		get ФизическаяЗащита() {
			return this.params.find(f => f.type == EParamType.ФизическаяЗащита);
		}

		get МагическаяЗащита() {
			return this.params.find(f => f.type == EParamType.МагическаяЗащита);
		}

		get ПовышениеУрона() {
			return this.params.find(f => f.type == EParamType.ПовышениеУрона);
		}

		get ПовышениеЗащиты() {
			return this.params.find(f => f.type == EParamType.ПовышениеЗащиты);
		}

		get Баффы() {
			return this.params.find(f => f.type == EParamType.Баффы);
		}

	}
	Sereborod.Hero = Hero;

	class Battle {
		constructor(db_battle) {
			this.step = 1;
			console.log("12345");
		}

		get heroes_alive() {
			return this.heroes.filter(h => h.alive);
		}

		get heroes_not_alive() {
			return this.heroes.filter(h => !h.alive);
		}

		to_log(message) {
		}

		decrease_time() {
			var GValue = {};
			for (var hero of this.heroes_alive) {
				for (var param of hero.params) {
					var values;
					for (var i = 0; i < values.Count; i++)
						values[i].Time--;
					values = param.value.filter(w => w.type == GValue.ValueType.АктивнаяСпособность && w.time == 0);
					for (var value of values)
						param.value.splice(param.value.findIndex(v => v == value));
				}
				for (var skill of hero.abilities_active.filter(a => a.cooldown > 0)) {
					skill.cooldown_timer--;
				}
				for (var pa of hero.abilities_passive.filter(p => p.custom && p.d > 0)) {
					pa.d--;
				}
				hero.abilities_passive = hero.abilities_passive.filter(p => !(p.custom && p.d == 0));
			}
		}

		check_death() {
			var heroes_for_delete = this.heroes.filter(h => Sereborod.CheckDeath[h.death_type]());
			for (var hero of heroes_for_delete) {
				hero.slots = null;
				hero.alive = false;
				this.to_log("Персонаж <span class='hero'>" + hero.name + "</span> скончался.");
			}

		}

		do_all_actions() {
			for (var step = 0; step < 3; step++) {
				this.step = step;
				this.do_continue_action_skills();
				this.decrease_time();
				this.battle_skills = [...this.get_all_skills(step)];
				for (var skill of this.battle_skills.sort(o => o.skill.naked.priority).reverse()) {
					skill.skill.apply(this, skill.targets);
					skill.skill.cooldown_timer = skill.skill.cooldown;
				}
				this.check_death();
				if (this.check_end())
					return;
			}
			for (var hero of this.heroes_alive) {
				hero.energy += 3;
				if (hero.energy > hero.max_energy)
					hero.energy = hero.max_energy;
			}
			this.next_round();
			return this;
		}

		check_end() {
			if (this.heroes_alive.length <= 1) {
				this.is_end = true;
				if (this.heroes_alive.length == 1) {
					var winSide = this.heroes.filter(h => h.side == this.heroes_alive[0].side);
					var loseSide = this.heroes.filter(h => h.side != this.heroes_alive[0].side);
					for (var hero of winSide) {
						Sereborod.DbHero.instant_add_experience(hero.id, 50);
						this.to_log("<span class='hero'>" + hero.name + "</span> +50 опыта");
					}
					for (var hero of loseSide) {
						Sereborod.DbHero.instant_add_experience(hero.id, 20);
						this.to_log("<span class='hero'>" + hero.name + "</span> +20 опыта");
					}
				}
				else {
					for (var hero of this.heroes) {
						Sereborod.DbHero.instant_add_experience(hero.id, 33);
						this.to_log("<span class='hero'>" + hero.name + "</span> +33 опыта");
					}
				}
				return true;
			}
			return false;
		}

		next_round() {
			for (var gHero of this.heroes_alive)
				gHero.slots = null;
			this.current_round++;
		}

		do_continue_action_skills() {
			for (var hero of this.heroes_alive) {
				if (hero.Баффы.value.length > 0) {
					for (var value of hero.Баффы.value) {
						if (value.continue_action)
							value.skill.naked.continue_action(value.continue_data);
					}
				}
			}
		}

		check_targets(sd) {
			if (sd.Targets != null) {
				for (var hero of sd.Targets) {
					if (hero == null)
						return false;
					if (!sd.Skill.Owner.Battle.Heroes.Contains(hero))
						return false;
					switch (sd.Skill.TargetType) {
						case Sereborod.ESkillTargetType.enemy:
							if (!sd.Skill.Owner.Battle.heroes_alive.filter(w => w.Side != sd.Skill.Owner.Side).Contains(hero))
								return false;
							break;
						case Sereborod.ESkillTargetType.ally:
							if (!sd.Skill.Owner.Battle.heroes_alive.filter(w => w.Side == sd.Skill.Owner.Side).Contains(hero))
								return false;
							break;
						case Sereborod.ESkillTargetType.ally_no_cur:
							if (!sd.Skill.Owner.Battle.heroes_alive.filter(w => w.Side == sd.Skill.Owner.Side).Contains(hero))
								return false;
							if (sd.Skill.Owner == hero)
								return false;
							break;
					}
				}
			}
			return true;
		}

		*get_all_skills(step) {
			for (var hero of this.heroes_alive) {
				if (hero.slots.length < 3)
					continue;
				var slot = hero.slots[step];
				if (slot == null || slot.id == 0)
					continue;
				var skill = hero.abilities_active.find(a => a.id == slot.id);
				if (skill.action_points == 2) {
					if (step == 2)
						continue;
					if (step == 1)
						hero.slots[2] = null;
					if (step == 0)
						hero.slots[1] = null;
				}
				if (skill.action_points == 3) {
					if (step == 0) {
						hero.slots[1] = null;
						hero.slots[2] = null;
					}
					else
						continue;
				}
				if (skill.mana_cost <= hero.energy)
					hero.energy -= skill.mana_cost;
				else
					continue;
				if (skill.cooldown_timer > 0)
					continue;
				if (skill != null) {
					var sd = { targets: slot.targets, skill: skill };
					if (this.check_targets(sd))
						yield sd;
				}
			}
		}

		do_event_actions(event_name, event_params) {
			for (var hero of this.heroes) {
				for (var pa of hero.abilities_passive.filter(p => p.naked.events.some(ev => ev == event_name))) {
					pa.apply(this, event_name, event_params);
				}
			}
		}

	}
	Sereborod.Battle = Battle;


	Sereborod.Actives = {
		"1": {
			id: 1,
			class: "maroder",
			name: "Удар мародёра",
			description: "Наносит ${1} ед. урона + ${2} от силы. В случае если удар завершиться смертью цели, то мародёр получит за бой золота на %{3} больше.",
			params: [[5, 10, 15], [10], [0.10]],
			mana_cost: [4],
			action_points: [1],
			cooldown: [0],
			step_duration: [0],
			minimal_round: [0],
			type: Sereborod.ESkillType.physicalAttack,
			targets: [{ unit_number: 1, target_type: Sereborod.ESkillTargetType.enemy }],
			up_level: [1, 5, 10],
			apply(battle, targets, $) {
			},
		},
		"2": {
			id: 2,
			class: "maroder",
			name: "Стремительная атака",
			description: "Наносит урон в размере ${1}% от силы, в случае если у противника остаётся менее 30-60%(основано на Удаче) здоровья, то парализует цель на ${2} очко действия.",
			params: [[20, 35, 50], [1]],
			mana_cost: [6],
			action_points: [2],
			cooldown: [1],
			type: Sereborod.ESkillType.physicalAttack,
			targets: [{ unit_number: 1, target_type: Sereborod.ESkillTargetType.enemy }],
			up_level: [2, 6, 12],
			apply(battle, targets, $) {
			},
		}
	};

	Sereborod.CheckDeath = {
		default(battle_data_hero, battle = null) {
			return battle_data_hero.abilities_passive[35].health <= 0;
		},
		raid_boss(battle_data_hero, battle = null) {
			return false;
		},
	};

	Sereborod.Modes = {
		"default": {
			name: "Тестовый",
			check_requirements(hero, battle) {
				return true;
			},
			is_mode_ready(battle) {
				return (battle.heroes.length == 2);
			}
		},
		"standard": {
			name: "Обычный",
			check_requirements(hero, battle) {
				return true;
			},
			is_mode_ready(battle) {
				return (battle.heroes.length == battle.params);
			}
		}
	};

	Sereborod.Passives = {
		"1": {
			id: 1,
			name: "Вампиризм",
			description: "Восстанавливает ${logic.byLevelParam()*100} от нанесённого урона физическими атаками в здоровье.",
			params: [[0.075, 0.150, 0.225]],
			events: ["ПослеНанесенияУронаПерсонажу"],
			apply(battle, eventType, eventParams, $) {
				return (eventType == "ПослеНанесенияУронаПерсонажу" && $.owner == eventParams.initiator)
					? ($.restoreHealth(eventParams.initiator, eventParams.initiator, Sereborod.ESkillType.physicalSkill, eventParams.Damage * $.byLevelParam()),
						true)
					: false;
			},
		},
		"2": {
			id: 2,
			name: "Критический удар",
			description: "Вероятность ${logic.byLevelParam()*100}% нанести в два раза больше урона физическими атаками.",
			events: ["ДоНанесенияУронаПерсонажу"],
			params: [[0.10, 0.15, 0.20]],
			apply(battle, eventType, eventParams, $) {
				return (eventType == "ДоНанесенияУронаПерсонажу" && $.owner == eventParams.initiator) && ($.random_f() < $.byLevelParam()) ? (eventParams["ПредполагаемыйУрон"] *= 2,
					$) : false;
			},
		},
		"3": {
			id: 3,
			name: "Дьявольский удар",
			description: "Вероятность 2-7%(основано на Ловкости) нанести в ${logic.byLevelParam(2, [[3,'раза'],[4,'раза'],[5,'раз']])} больше урона физическими атаками.",
			events: ["ДоНанесенияУронаПерсонажу"],
			params: [[3, 4, 5]],
			apply(battle, eventType, eventParams, $) {
				return ((eventType == "ДоНанесенияУронаПерсонажу" && $.owner == eventParams.initiator) &&
					($.random() < $.baseOnParam($.getHeroParam(Sereborod.EParamType.Ловкость), [
						[0, 2],
						[30, 3],
						[60, 4],
						[90, 5],
						[120, 6],
						[150, 7]
					]))) ? (eventParams.modifiers.push($),
					eventParams["ПредполагаемыйУрон"] *= $.byLevelParam(),
					true) : false;
			},
		},
		"35": {
			id: 35,
			name: "Здоровье",
			description: "Увеличивает на каждую единицу Выносливости здоровье на {0} ед.",
			params: [[1.22, 1.88, 2.54]],
			initialize(skill, hero) {
				hero.max_health = 15 + hero.params.find(f => f.type == Sereborod.EParamType.Выносливость).value[0].value * skill.byLevelParam();
			},
		}
	};
})(Sereborod || (Sereborod = {}));
module.exports = Sereborod;