﻿const sereborod = require('./sereborod.js');
const loki = require('lokijs');

let port = process.env.port;
let server_name = ``;
let supervisor_address = process.env.number.supervisor_address;
let battle_id = process.env.battle_id;

let battle_server = require('socket.io');
//server.set("loglevel", 1);

battle_server.use(async function(socket, next) {
	//socket.conn.remoteAddress, socket.handshake.address
	if(socket.handshake.address == supervisor_address);
	
	let token = socket.handshake.query.token;
	if (!token)
		return auth.fail("Токен отсутствует", data, accept);
		
	var [uid, hid] = (await client.getAsync(token)).split(",").map((v) => v-0);
	if(uid && hid) {
		socket.hero_id = hid;
		next();
	}
	else {
		next("Неверный токен");
	}
});
	
let db = null;
battle_server.on('connection', function(socket) {
	console.log(`a hero #[${socket.hero_id}] connected at socket ${socket.id}`);
	socket.on('battle/start', async function(battle, fn){
		var db = new loki('battle_' + battle.id);	
		var hero_data = db.addCollection('hero_data', { unique: ['hero_id'], autoupdate: true }); // { hero_id, team_id, params, slot, skills ... }
		var game_state = db.addCollection('game_state');
		game_state.insert({ round: 1 });
		for(var i = 0; i < battle.heroes.length; i++)
			hero_data.insert({ 
				hero_id: battle.heroes[i].id,
				team_id: (i % 2) + 1
			});
		battles.set(battle.id, db);
	});
	socket.on('game/slot/set', async function(slot) {
		db.query(`UPDATE battles SET data = data || '[{"id":$<id^>, "name":"$<name^>"}]'::jsonb WHERE id = $<battle_id>`,{
			id: hero.id,
			name: hero.name,
			battle_id: battle.id,
		});
	});
});

battle_server.listen(port, function() {
	console.log(`sereborod battle-server/battle started at port ${port} with supervisor supervisor_address`);
});