﻿const argv = require('yargs').argv,
	loki = require('lokijs'),
	child_process = require('bluebird').promisifyAll(require('child_process')),
	ss = require('socket.io-stream'),
	http = require('http'),
	fs = require('fs');

if(!argv.port)
	throw new Error("port not defined");

if(!argv.game_server)
	throw new Error("game_server not defined");

let port_diaposon = [20000, 22000];

let db = new loki('supervisor');
let servers = db.addCollection('servers');


/*let game_server = require('socket.io-client')(argv.game_server_address);
game_server.on('connect', () => console.log('connected to game_server'));
game_server.on('disconnect', () => console.log('disconnected to game_server'));
game_server.on('connection', (socket) => {
	ss(socket).on('download-sereborod')
	console.log('disconnected to game_server');
});*/

/*ss(game_server).on('disconnect', () => console.log('disconnected to game_server'));

ss(game_server).emit('download-sereborod', sereborod_version, function(stream) {
	
});*/
const server = http.createServer();
const supervisor = require('socket.io')(server);

let game_server_socket_id = null;

supervisor.use(async function(socket, next) {
	if(socket.conn.remoteAddress == game_server_address || socket.handshake.address == game_server_address) {
		game_server_socket_id = socket.id;
		//io.to({ id: socketId }).emit('myevent', {foo: 'bar'});
		//io.to('/#' + socketId).emit('myevent', {foo: 'bar'});
		next();
	} else
		next('fail authorization');
});

function download_sereborod(version) { //version - это hash
	return new Promise((resolve, reject) => {
		var file = fs.createWriteStream("sereborod.js");
		var request = http.get(game_server_address + "/$/sereborod.js", (response) => {
			response.pipe(file);
			file.on('finish', () => {
				file.close(() => {
					resolve();
				});
			});
		}).on('error', (err) => {
			fs.unlink(dest);
		});
	});
}

supervisor.on('connection', function(socket) {
	console.log(`game_server connected at socket ${socket.id}`);
	socket.on('battle/start', async function(battle, fn){
		child_process.exec( '"' + process.execPath + '" battle.js', {env: { number: 1234 } }, function(error, stdout, stderror) {
			if(error) throw error;
			console.log(stdout);
			console.log(stderror);
        });
		servers.insert();
	});
});

server.listen(argv.port, function() {
	console.log(`[[sereborod battle-server/supervisor]] started on port ${argv.port}`);
});